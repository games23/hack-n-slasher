﻿using UnityEngine;

namespace Assets.Scripts.Actors
{
    public class VfxPlayer : MonoBehaviour
    {
        public ParticleSystem Blood;
        public ParticleSystem DashTrail;
        public ParticleSystem SpecialAttack;

        public void PlayTrail() => DashTrail.Play();
        public void StopTrail() => DashTrail.Stop();

        public void Bleed() => Blood.Play();

        public void PlaySpecial() => SpecialAttack.Play();
    }
}
