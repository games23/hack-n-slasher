﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.Actors
{
    public class CollisionDetection : MonoBehaviour
    {
        public LayerMask CollisionLayer;
        public float YOffset;
        public float LookAheadFactor;

        public bool CanMoveTowards(Vector3 direction)
        {
            direction = direction.normalized * LookAheadFactor;
            direction.y = YOffset;

            var rayOrigin = transform.position;
            rayOrigin.y += YOffset;
            direction.y = rayOrigin.y;
            var ray = new Ray(rayOrigin, direction);
            var hit = new RaycastHit();
            return !Physics.Raycast(ray, out hit, direction.magnitude, CollisionLayer);
        }
    }
}
