﻿using UnityEngine;

namespace Assets.Scripts.Actors.PlayingCharacter
{
    public class Dash : MonoBehaviour
    {
        public float Duration;
        public float Speed;
        private CharacterMovement movement;

        private float elapsed;
        private CharacterView view;
        private Vector3 direction;
        private CollisionDetection detector;

        public void Initialize(CharacterMovement movement, CollisionDetection detector, CharacterView view)
        {
            this.movement = movement;
            this.detector = detector;
            this.view = view;
            enabled = false;
        }

        public void Do(Vector2 direction)
        {
            if (!enabled)
            {
                this.direction = new Vector3(direction.x, 0f, direction.y).normalized;
                movement.Disable();
                view.StartRoll();
                elapsed = Duration;
                enabled = true;
            }
        }

        void Update()
        {
            var moveVector = direction * Speed * Time.deltaTime;
            if (detector.CanMoveTowards(moveVector))
                transform.position += moveVector;

            elapsed -= Time.deltaTime;
            if(elapsed <= 0)
                OnDashFinised();
        }

        void OnDashFinised()
        {
            view.StopRoll();
            movement.Enable();
            enabled = false;
        }

        public void ForceStop() => OnDashFinised();
    }
}
