﻿using Assets.Scripts.Actors;
using Assets.Scripts.Actors.PlayingCharacter;
using Assets.Scripts.Directors;
using Assets.Scripts.UI;
using UnityEngine;

namespace Assets.Scripts.Player
{
    public class PlayerInitializer : MonoBehaviour
    {
        public PlayerInput Input;
        public Character Character;
        public ActorProvider ActorProvider;
        public HealthBar HealthBar;

        void Start()
        {
            Character.Initialize(Input, HealthBar, ActorProvider);
        }
    }
}
