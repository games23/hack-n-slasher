﻿using System.Linq;
using Assets.Scripts.Actions;
using Assets.Scripts.Actors;
using Assets.Scripts.Actors.PlayingCharacter;
using UnityEngine;

namespace Assets.Scripts.Player
{
    public class PlayerInput : MonoBehaviour
    {
        private CharacterMovement movement;
        private CharacterActions actions;
        private Vector2 lastMovementVector;

        public void Initialize(CharacterMovement movement, CharacterActions actions)
        {
            this.movement = movement;
            this.actions = actions;
        }

        void Update()
        {
            if (Input.GetButtonDown("Attack"))
                actions.Attack();

            if (Input.GetButtonDown("Dash"))
                actions.Dash(lastMovementVector);

            var movementVector = new Vector2(
                Input.GetAxis("Horizontal"),
                Input.GetAxis("Vertical")
            );


            movementVector.y *= 1.5f;
            movement.Move(movementVector);
            actions.SetAttackDirectionTo(movementVector);

            if (movementVector.magnitude > 0)
                lastMovementVector = movementVector;
        }

        public void Disable() => 
            enabled = false;
    }
}