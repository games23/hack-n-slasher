﻿using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Actions;
using Assets.Scripts.Actors.AI;
using Assets.Scripts.Actors.PlayingCharacter;
using UnityEngine;

namespace Assets.Scripts.Directors
{
    public class ActorProvider : MonoBehaviour
    {
        public Character Hero;

        private IEnumerable<Monster> monsters;
        
        public void Initialize(IEnumerable<Monster> monsters) => 
            this.monsters = monsters;

        public IEnumerable<Monster> GetMonstersInArea(ConeArea area) =>
            monsters.Where(a => area.IsInArea(a.transform.position));
    }
}
